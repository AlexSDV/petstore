package sdv.alexandre.proust.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class Cat extends Animal {

    @Column
    private String chipId;

    public Cat(String chipId) {
        this.chipId = chipId;
    }

    public Cat(LocalDate date, String couleur, String chipId) {
        super(date, couleur);
        this.chipId = chipId;
    }

    public Cat() {
    }

    public String getChipId() {
        return chipId;
    }

    public void setChipId(String chipId) {
        this.chipId = chipId;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "chipId='" + chipId + '\'' +
                '}';
    }
}
