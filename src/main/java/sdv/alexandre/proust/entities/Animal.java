package sdv.alexandre.proust.entities;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public abstract class Animal {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column
    private LocalDate date;

    @Column
    private String couleur;

    @ManyToOne
    @JoinColumn(name="PS_ID")
    private PetStore petStore;

    public Animal() {
    }

    public Animal(Long id, LocalDate date, String couleur) {
        this.id = id;
        this.date = date;
        this.couleur = couleur;
    }

    public Animal(LocalDate date, String couleur, PetStore petStore) {
        this.date = date;
        this.couleur = couleur;
        this.petStore = petStore;
    }

    public Animal(LocalDate date, String couleur) {
        this.date = date;
        this.couleur = couleur;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "id=" + id +
                ", date=" + date +
                ", couleur='" + couleur + '\'' +
                '}';
    }
}
