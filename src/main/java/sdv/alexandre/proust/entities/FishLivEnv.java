package sdv.alexandre.proust.entities;


public enum FishLivEnv {
    FRESH_WATER, SEA_WATER;
}
