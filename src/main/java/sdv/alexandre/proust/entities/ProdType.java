package sdv.alexandre.proust.entities;

public enum ProdType {
    FOOD, ACCESSORY, CLEANING;
}
