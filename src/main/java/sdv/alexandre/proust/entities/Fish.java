package sdv.alexandre.proust.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import java.time.LocalDate;

@Entity
public class Fish extends Animal{

    @Enumerated
    private FishLivEnv fishLivEnv;

    public Fish(FishLivEnv fishLivEnv) {
        this.fishLivEnv = fishLivEnv;
    }

    public Fish() {
    }

    public Fish(LocalDate date, String couleur, PetStore petStore, FishLivEnv fishLivEnv) {
        super(date, couleur, petStore);
        this.fishLivEnv = fishLivEnv;
    }

    public Fish(LocalDate date, String couleur, FishLivEnv fishLivEnv) {
        super(date, couleur);
        this.fishLivEnv = fishLivEnv;
    }

    public FishLivEnv getFishLivEnv() {
        return fishLivEnv;
    }

    public void setFishLivEnv(FishLivEnv fishLivEnv) {
        this.fishLivEnv = fishLivEnv;
    }

    @Override
    public String toString() {
        return "Fish{" +
                "fishLivEnv=" + fishLivEnv +
                '}';
    }
}
